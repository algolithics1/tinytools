
import numpy as np
import  sets_simi as setsim
# import modules.sets_simi.sets_simi as setsim


ints_fin=[ set(np.array([1,2,3,4,5,6,7,8,9,0])), set(np.array([1,2,3,4,5,6,7,8])), set(np.array([11,12,13,4,5,6,7,8])), set(np.array([3,4,5,8,9,0])), set(np.array([1,2,3,4,5,6,7,8,9,0] )) ]

print(setsim.overlapLen(ints_fin[0], ints_fin[1] ))
print(setsim.setsSimilarity(ints_fin[0], ints_fin[1] ))
print(setsim.prct_set1_in_set2(ints_fin[0], ints_fin[1] ))