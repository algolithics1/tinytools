# findSimilarSets(float x, int maxSets, set baseSet, list targetSets, dict result)
# overlapLen(set set1, set set2)
# setsSimilarity(set set1, set set2)
# prct_set1_in_set2(set set1, set set2)


cimport cython
@cython.boundscheck(False)
@cython.wraparound(False)
	

########
# also can be done common= set1.intersection(set2)
def overlapLen(set set1, set set2): # usage of this fun above is 5% slower
	return len(set1 & set2)	



###
# sets similarity 
def setsSimilarity(set set1, set set2):
	return 2.0*len(set1 & set2)/(len(set1)+len(set2))
	
	

###
# set1 % in set2 
# if set1 is all in set2 return 1
def prct_set1_in_set2(set set1, set set2):
	return 1.0*len(set1 & set2)/len(set1)	
	
	
	
############	
# find max maxSets sets from list targetSets that overlap with baseSet with similarity>= x 
# eg x=0.8 maxSets=1
# maxSets<0 to turn the cut off - will find all 

def findSimilarSets(float x, int maxSets, set baseSet, list targetSets, dict result):  

	if x>1:
		print('Wrong x>1 in findSimilarSets')
		return
	elif x<=0:
		print('Wrong x<=0 in findSimilarSets')
		return
	
	cdef int i, N, set1len, set2len, aX 
	cdef float rate , setlenratio, optiratio, len1f
	
	aX=min(maxSets-1,0)
	set1len=len(baseSet)  
	N = len(targetSets) 
	optiratio=(2.0/x-1)*1.01
	len1f=float(set1len)
	
	for i in range(N):
	
		if aX>=maxSets :  #print('early fini')
			return
	
		set2len=len(targetSets[i])  
		setlenratio=set2len/len1f
		if setlenratio>optiratio or setlenratio*optiratio<1: 
			continue
		
		rate=2.0*len(baseSet & targetSets[i])/(set2len+set1len)   #rate=2.0*overlapLen(baseSet, targetSets[i])/(set2len+set1len)  
		
		if rate>=x:
			result['rate'][i]=rate
			if maxSets>0:
				aX+=1
		 

	